---
marp: true
paginate: true
theme: default
---
<!-- _backgroundColor: #292961 -->
<!-- _color: white -->
![bg left:40% 90%](assets/gitlab-logo-white-stacked-rgb.png)

# Introduction à Gitlab CI


-emmanuel.braux@imt-atlantique.fr-

---
<!-- _backgroundColor: #292961 -->
<!-- _color: white -->

![bg left:30% 80%](assets/gitlab-logo-gray-stacked-rgb.png)

# Introduction à Gitlab CI

- **Introduction**
- **Jobs et Pipelines**
- **Les Runners**
- **Les Variables**
- **Les Artefacts**
- **La Registry Docker**
- **Les Gitlab Pages**

---

# Licence informations

Auteur : emmanuel.braux@imt-atlantique.fr

Cette présentation est sous License Créative Commons 3.0 France (CC BY-NC-SA 3.0 FR)
Selon les options : Attribution - Pas d’Utilisation Commerciale - Partage dans les Mêmes Conditions.

![cc-by-nc-sa](img/cc-by-nc-sa.png)


---
<!-- _backgroundColor: #292961 -->
<!-- _color: white -->

![bg left:30% 80%](assets/gitlab-logo-gray-stacked-rgb.png)

# Introduction à Gitlab CI

- ## > Introduction
- **Jobs et Pipelines**
- **Les Runners**
- **Les Variables**
- **Les Artefacts**
- **La Registry Docker**
- **Les Gitlab Pages**


---

![bg left:25% 80%](assets/gitlab-logo-gray-stacked-rgb.png)
# Gitlab

Plateforme développement logiciel: 
- Gestions de dépôt Git
- Gestion de suivi de bug, wiki
- Gestion de packages (Maven, Npm, PyPi, ...)
- Registry docker
- Gestion d'infrastructure (k8S, ... )
- Gestion de chaîne d'intégration continue
- ...

*[https://about.gitlab.com/features/](https://about.gitlab.com/features/)*

---
![bg left:25% 80%](assets/gitlab-logo-gray-stacked-rgb.png)

# Gitlab
- [https://gitlab.com/](https://gitlab.com/)
- Gitlab est Opensource, et peut être installé "on premise"
- Les fonctionnalités de base sont gratuites
- Différents niveaux de fonctionnalités pour développeurs et entreprises sont proposés
  
*[https://about.gitlab.com/pricing/](https://about.gitlab.com/pricing/)*


---
# **CI**/CD :  Continuous integration

- Construire, tester et diffuser un logiciel plus rapidement
- "Commit" du code dans le dépôt, et notification
- Lancement de l'intégration du code, pour le vérifier :
  - Test de build du code : qualité de code, ...
  - Test du code : intégration et acceptance
  - ...

---
# CI/**CD**:  Continuous Delivery / Deployment

**Déployer les modifications de façon systématique et contrôlée**

- Livraison fréquentes et automatisées
- Toujours une version disponible pour être utilisée par les clients (releasable)
- Possibilité de revenir en arrière si besoin
- Réduire les coûts, le temps et les risques inhérents aux changements.

- _**Delivery**_ : le déploiement reste manuel.
- _**Deployment**_ : l'application est déployée automatiquement.

---
# Gitlab-CI

- Ensemble d'outils de CI/CD intégrés à Gitlab
- Déclencher des actions sur des événements : 
  - Lors des "push" de code par les développeurs
  - ...
- Intégrer des fonctionnalités avancées :
  - Gestion des tests
  - Gestion des merge/request 
  - ...
- Faciliter la mise en place des workflows de gestion de déploiement

---
# .gitlab-ci.yaml

Fichier Manifest **.gitlab-ci.yaml**
- Décrivant un ensemble d'étapes : les **stages**
- Chaque stage est composées de taches : les **jobs**
- Chaque job contient un **script**
- Les scripts sont exécutés par un **Runner**
 
---
#  Limitation des runners sur Gitlab.com

- Gitlab propose des Runners partagés (instance Runner)
- Ces runners ont été utilisés pour miner de la crypto monnaie.
- Pour les comptes créés après mai 2021 : obligation de saisir des coordonnées bancaires
- Alternative : déployer ses propres runners (simple)

[https://about.gitlab.com/blog/2021/05/17/prevent-crypto-mining-abuse/](https://about.gitlab.com/blog/2021/05/17/prevent-crypto-mining-abuse/)

---
![bg left:35% 80% drop-shadow:0,5px,10px,rgba(0,0,0,.4)](./img/menu-build.png)


# Accès aux fonctionnalités de CI/CD

Dans l'interface de Gitlab, menu "Build"
- Edition du .gitlab-ci.yaml
- Accès aux pipelines
- Accès aux jobs
- ...

---
![bg left:35% 80% drop-shadow:0,5px,10px,rgba(0,0,0,.4)](img/menu-cicd.png)

# Paramétrage des fonctionnalités de CI/CD

Dans l'interface de Gitlab, menu "Settings > CI/CD"

- Runner
- Variables
- Artefacts
- ...

---
# Exemple de pipeline

- Création d'un job "job_test"
- Utilisation du stage "test", un des 3 stages par défaut (build, test, deploy)
- Lancement de commandes Linux
  
```yaml
job_test:
  stage: test
  script:
    - echo "Hello world"
    - date
```

*[https://docs.gitlab.com/ee/ci/examples/index.html](https://docs.gitlab.com/ee/ci/examples/index.html)*

---
<!-- _class: invert -->
<!-- _color: white -->

# LAB "Lancer un premier Pipeline"

- Écrire un pipeline "Hello World"
- Analyser et comprendre l'execution du pipeline

---

<!-- _backgroundColor: #292961 -->
<!-- _color: white -->

![bg left:30% 80%](assets/gitlab-logo-gray-stacked-rgb.png)

# Introduction à Gitlab CI

- **Introduction**
- ## > Jobs et Pipelines
- **Les runners**
- **Les Variables**
- **Les Artefacts**
- **La registry Docker**
- **Les Gitlab Pages**

---
# Les jobs

- Elements de base de GitLab CI
- Un pipeline de CI/CD doit contenir au moins un job
- Un job doit contenir au moins un élément "script" à exécuter
- De très nombreuses options sont disponibles pour ajuster le fonctionnement des jobs, leur environnement, ... 
  
*[https://docs.gitlab.com/ee/ci/yaml/#job-keywords](https://docs.gitlab.com/ee/ci/yaml/#job-keywords)*
*[https://docs.gitlab.com/ee/ci/yaml/#script](https://docs.gitlab.com/ee/ci/yaml/#script)*


---
# Les stages 

- Les jobs sont organisés en "stages"
- 3 stages par défaut : **build**, **test**, **deploy**
- Des stages personnalisés peuvent être définis simplement
- Les stages s'exécutent de façon séquentielle
- Les jobs dans les stages s'exécutent en parallèle
  
*[https://docs.gitlab.com/ee/ci/yaml/#stages](https://docs.gitlab.com/ee/ci/yaml/#stages)*

---
# Exemple de pipeline Multi-stages / Multi-jobs : 

![h:300 drop-shadow:0,5px,10px,rgba(0,0,0,.4)](img/pipeline-multi-stage-jobs.png)

---
# Autres fonctionnalités

- **Etapes manuelles** : par exemple pour la mise en production d'une application.
- **Gestion des erreurs**: arrêt ou non d'un pipeline en cas d'erreur sur un job
- ...

*[https://docs.gitlab.com/ee/ci/yaml/#when](https://docs.gitlab.com/ee/ci/yaml/#when)*
*[https://docs.gitlab.com/ee/ci/yaml/#allow_failure](https://docs.gitlab.com/ee/ci/yaml/#allow_failure)*



---
<!-- _class: invert -->
<!-- _color: white -->

# LAB "Gestion des pipeline et jobs"

- Définir des "Stages" et des "Jobs"
- Comprendre leur ordonnancement 
- Utiliser des déclenchements manuels
- Gérer les erreurs

---
<!-- _backgroundColor: #292961 -->
<!-- _color: white -->

![bg left:30% 80%](assets/gitlab-logo-gray-stacked-rgb.png)

# Introduction à Gitlab CI

- **Introduction**
- **Jobs et Pipelines**
- ## > Les runners
- **Les Variables**
- **Les Artefacts**
- **La registry Docker**
- **Les Gitlab Pages**


---
# Les runners

Les job de CI/CD sont lancés sur des runners.

Différents niveaux d'accès aux Runners :

- **Instance runners** : runner globaux, gérés au niveau de la plateforme Gitlab
- **Group runners** : si votre projet fait partie d'un groupe, ce sont des runner dédiés au groupe
- **Project runners** : dédiés au projet, gérée par vous, ou le gestionnaire du projet. Ils peuvent être partagés entre plusieurs projets
  

> Les runners peuvent être exécutés sur n'importe quelle machine. La seule contrainte est un accès en http vers la plateforme Gitlab.

---
# Types de runners (Executors)

- **Docker** : lancement d'un conteneur Docker
- **Shell** : execute des commandes shell sur la machine hébergeant le runner
- **SSH** : connexion via SSH à une machine distante
- **Docker autoscaller / Kubernetes** : lancement de conteneur avec mécanismes d'autoscaling
- **Virtualbox** : interaction avec virtualbox, pour lancer des VMs
- **Instance** : interraction avec des Cloud Provider (AWS, GCP, AZURE, ...)
- **Custom** : développement personnalisé

> ### Dans la majorité des cas, on utilise des runners de type Docker

*[https://docs.gitlab.com/runner/executors/index.html](https://docs.gitlab.com/runner/executors/index.html)*

---
![bg left:30% 80% drop-shadow:0,5px,10px,rgba(0,0,0,.4)](img/settings-CI-CD.png)
# Liste des runners

  On retrouve la liste des runners disponibles dans la partie CI/CD des "settings" du projets

 ![h:200 drop-shadow:0,5px,10px,rgba(0,0,0,.4)](img/settings-runner.png)


---
# Execution des runners Docker

- Utilise le daemon Docker de l'hôte sur lequel est déployé le runner
- Une image par défaut est choisie lors de l'installation du Runner.
- L'image à utiliser peut être personnalisée pour chaque job, avec le mot clé `image:`

---
<!-- _class: invert -->
<!-- _color: white -->

# LAB "Personnaliser les runner"

- Configurer une image sur un job
- Déployer un runner dédié à votre projet


---

<!-- _backgroundColor: #292961 -->
<!-- _color: white -->

![bg left:30% 80%](assets/gitlab-logo-gray-stacked-rgb.png)

# Introduction à Gitlab CI

- **Introduction**
- **Jobs et Pipelines**
- **Les runners**
- ## > Les Variables
- **Les Artefacts**
- **La registry Docker**
- **Les Gitlab Pages**


---
# Les Variables

- Gitlab propose un mécanisme de variables qui peuvent ensuite être utilisées dans la chaîne de CI
- Il existe des variables pré-définies
    - Organisation interne de Gitlab 
    - Nom du projet, chemin du projet, ...
- Des variables personnalisées peuvent être définies.

*[https://docs.gitlab.com/ee/ci/variables/](https://docs.gitlab.com/ee/ci/variables/)*

---
# Variables personnalisées

Définition: 
- Dans l'interface de Gitlab : au niveau Projet ou Groupe
- Dans un pipeline : globalement, ou au niveau d'un job.

Possibilité de les masquer : valeur non visible dans les logs des jobs

Possibilité de leur donner des valeurs différentes en fonction de l'environnement (Prod, Dev, ...)

---
<!-- _class: invert -->
<!-- _color: white -->

# LAB "Utilisation des Variables"

- Utiliser des variables dans un un fichier .gitlab.yml
- Utiliser les variables prédéfinies dans Gitlab CI
- Définir des variables personnalisées au niveau de Gitlab 

---
<!-- _backgroundColor: #292961 -->
<!-- _color: white -->

![bg left:30% 80%](assets/gitlab-logo-gray-stacked-rgb.png)

# Introduction à Gitlab CI

- **Introduction**
- **Jobs et Pipelines**
- **Les runners**
- **Les Variables**
- ## > Les Artefacts
- **La registry Docker**
- **Les Gitlab Pages**


---
# Les Artifacts

- Chaque job est **indépendant**, et peut-être exécuté sur un Runner **différent**.
- Pour partager des données entre les jobs, il faut le faire de façon explicite, en utilisant les **artifacts**
- C'est en quelque sorte un espace de stockage entre les jobs.
- Un artefact ne permet le partage qu'entre les jobs de stages différents

---
# Déclaration d'un Artifact
![bg right :30% 70% drop-shadow:0,5px,10px,rgba(0,0,0,.4)](img/artifacts-menu.png)

```yaml
artifacts:
    paths:
      - public
``` 

Une nouvelle section apparaît dans l'interface de gestion de pipeline : 

---
# Optimisation de la gestion des Artifacts

- Les artifacts sont :
  - **Uploadés** sur le serveur Gitlab à la fin du job qui le génère
  - **Téléchargés** depuis le serveur Gitlab au début du job qui les utilise

- La rétention des artifacts peut être configurée : `expire_in`, ...
- Pour éviter les transferts inutiles, il est possible de gérer les transferts :  `dependencies`, `needs`, `exclude`, ...

*[https://docs.gitlab.com/ee/ci/yaml/#artifacts](https://docs.gitlab.com/ee/ci/yaml/#artifacts)*


---
<!-- _class: invert -->
<!-- _color: white -->

# LAB "Utilisation des Artifacts"

- Partager des ressources entre des jobs

---
<!-- _backgroundColor: #292961 -->
<!-- _color: white -->

![bg left:30% 80%](assets/gitlab-logo-gray-stacked-rgb.png)

# Introduction à Gitlab CI

- **Introduction**
- **Jobs et Pipelines**
- **Les runners**
- **Les Variables**
- **Les Artefacts**
- ## > La registry Docker
- **Les Gitlab Pages**


---
# La registry Docker

- Gitlab met à disposition de chaque projet une registry Docker
- Elle peut être publique, ou privée
- Des variables prédéfinies sont disponibles : `$CI_REGISTRY_USER`, `$CI_REGISTRY_PASSWORD`, `$CI_REGISTRY`, ...
- Elle peut être utilisée en dehors du mécanisme de CI
- Dans le cas d'une registry privée, il est possible de générer des token d'accès, en lecture/écriture, ou en lecture seule

*[https://docs.gitlab.com/ee/user/packages/container_registry/](https://docs.gitlab.com/ee/user/packages/container_registry/)*

---  
# Exemple de job de CI : build d'une image Docker

```yaml
build:docker:
  image: docker:24.0
  variables:
    DOCKER_DRIVER: overlay2
    DOCKER_TLS_CERTDIR: ""
  services:
    - name: docker:24.0-dind
      alias: docker
script:
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
    - docker build -t $CI_REGISTRY/group/project/image:latest .
    - docker push $CI_REGISTRY/group/project/image:latest
```
*[https://docs.gitlab.com/ee/user/packages/container_registry/build_and_push_images.html](https://docs.gitlab.com/ee/user/packages/container_registry/build_and_push_images.html)*

---
<!-- _class: invert -->
<!-- _color: white -->

# LAB "Utilisation des La registry Docker"

- Création d'une Image Docker dans la chaîne  de CI
- Rendre cette image disponible dans la registry du projet

---
<!-- _backgroundColor: #292961 -->
<!-- _color: white -->

![bg left:30% 80%](assets/gitlab-logo-gray-stacked-rgb.png)

# Introduction à Gitlab CI

- **Introduction**
- **Jobs et Pipelines**
- **Les runners**
- **Les Variables**
- **Les Artefacts**
- **La registry Docker**
- ## > Les Gitlab Pages


---
# Les Gitlab Pages

- Gitlab met à disposition de chaque projet un espace de publication de contenu : les "pages"
- Accessible via une url personnalisée, de type `https://<projet>.gitlab.io`
- Il suffit de définir un job **"pages"**, qui écrit du contenu dans un artefact **"public"**


---
<!-- _class: invert -->
<!-- _color: white -->

# LAB "Utilisation des Gitlab Pages"

- Publier du contenu à partir d'un pipeline de CI/CD

---
<!-- _backgroundColor: #292961 -->
<!-- _color: white -->

![bg left:30% 80%](assets/gitlab-logo-gray-stacked-rgb.png)

# Introduction à Gitlab CI

## Sources et 
## Informations complémentaires

---


# Sources
- [https://docs.gitlab.com/ee/ci/quick_start/index.html](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [https://blog.eleven-labs.com/fr/introduction-gitlab-ci/](https://blog.eleven-labs.com/fr/introduction-gitlab-ci/)

