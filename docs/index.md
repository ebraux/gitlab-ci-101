---
hide:
  - navigation
  - toc
---


# Gitlab-CI 101

Introduction à Gitlab CI, concepts de base et prise en main

---

Slides de la présentation:

- Version en ligne: [gitlab-ci-101.html](gitlab-ci-101.html)
- version PDF: [gitlab-ci-101.pdf](gitlab-ci-101.pdf)


---

Les labs correspondants à cette session sont disponibles :

- version en ligne : [https://ebraux.gitlab.io/gitlab-ci-labs/](https://ebraux.gitlab.io/gitlab-ci-labs/)
- dépôt Gitlab : [https://gitlab.com/ebraux/gitlab-ci-labs](https://gitlab.com/ebraux/gitlab-ci-labs)

---
![image alt <>](assets/gitlab-logo-white-stacked-rgb.png)


